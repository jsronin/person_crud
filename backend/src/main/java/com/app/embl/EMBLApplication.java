package com.app.embl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan("com.app.embl")
@EnableSwagger2
public class EMBLApplication {

    public static void main(String[] args) {
        SpringApplication.run(EMBLApplication.class, args);
    }

}
