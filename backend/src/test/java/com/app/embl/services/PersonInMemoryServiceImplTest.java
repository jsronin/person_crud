package com.app.embl.services;

import com.app.embl.dtos.PersonDto;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This is person in memory service tests
 */
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PersonInMemoryServiceImplTest {

    @Autowired
    PersonService personService;

    @BeforeAll
    public static void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    @Order(1)
    void findAll() {
        assertEquals(personService.findAll().size(), 2);
    }

    @Test
    @Order(2)
    void find() throws Exception {
        assertThrows(Exception.class, () -> personService.find(3L));
        assertEquals(personService.find(1L).getAge(), "29");
    }

    @Test
    @Order(3)
    void addPerson() throws Exception {
        PersonDto person = new PersonDto();
        person.setFirst_name("Joe");
        person.setLast_name("Farmer");
        person.setAge("55");
        assertEquals(personService.addPerson(person).getAge(), "55");
        assertEquals(personService.findAll().size(), 3);
    }

    @Test
    @Order(4)
    void updatePerson() throws Exception {
        PersonDto person = new PersonDto();
        person.setId(4L);
        person.setFirst_name("Joe");
        person.setLast_name("Farmer");
        person.setAge("85");
        assertThrows(Exception.class, () -> personService.updatePerson(person));
        person.setId(3L);
        assertEquals(personService.updatePerson(person).getAge(), "85");
    }

    @Test
    @Order(5)
    void deletePerson() throws Exception {
        assertThrows(Exception.class, () -> personService.deletePerson(4L));
        assertEquals(personService.deletePerson(3L), true);
    }

}