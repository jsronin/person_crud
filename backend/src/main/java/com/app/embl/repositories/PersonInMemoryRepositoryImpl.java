package com.app.embl.repositories;

import com.app.embl.models.Person;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is an in memory implementation of person repository
 */
@Repository
public class PersonInMemoryRepositoryImpl implements PersonRepository {

    public static Map<Long, Person> persons = initRepository();
    public static Long LAST_ID = persons.keySet().stream().max(Long::compare).orElse(1L);

    public static Map<Long, Person> initRepository() {
        Map<Long, Person> persons = new HashMap<>();
        Person person = new Person();
        person.setId(1L);
        person.setFirstName("John");
        person.setLastName("Keynes");
        person.setAge("29");
        person.setFavouriteColour("red");
        persons.put(person.getId(), person);
        person = new Person();
        person.setId(2L);
        person.setFirstName("Sarah");
        person.setLastName("Robinson");
        person.setAge("54");
        person.setFavouriteColour("blue");
        persons.put(person.getId(), person);
        return persons;
    }

    public static Long getNextId() {
        LAST_ID += 1L;
        return LAST_ID;
    }

    @Override
    public List<Person> findAll() {
        List<Person> personList = new ArrayList<>();
        for (Long id : persons.keySet()) {
            personList.add(persons.get(id));
        }
        return personList;
    }

    @Override
    public Person find(Long id) throws Exception {
        if (persons.get(id) != null) {
            return persons.get(id);
        } else {
            throw new Exception("Entity Not Found");
        }
    }

    @Override
    public Person addPerson(Person person) throws Exception {
        if (persons.get(person.getId()) == null) {
            person.setId(getNextId());
            persons.put(person.getId(), person);
        } else {
            throw new Exception("Duplicate Entity");
        }
        return person;
    }

    @Override
    public Person updatePerson(Person person) throws Exception {
        if (persons.get(person.getId()) != null) {
            persons.put(person.getId(), person);
        } else {
            throw new Exception("Entity Not Found");
        }
        return person;
    }

    @Override
    public Boolean deletePerson(Long id) throws Exception {
        if (persons.get(id) != null) {
            persons.remove(id);
        } else {
            throw new Exception("Entity Not Found");
        }
        return true;
    }
}
