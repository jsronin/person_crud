package com.app.embl.controllers;

import com.app.embl.dtos.PersonDto;
import com.app.embl.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

import static com.app.embl.mappers.PersonMapper.getResponseList;

/**
 * This is person entity crud controller
 */
@RestController
@CrossOrigin
@RequestMapping(path = "/v1/embl")
public class PersonController {

    @Autowired
    PersonService personService;

    /**
     * This API is for generation {"person":[person json object]}
     *
     * @return
     */
    @GetMapping(path = "/persons_embl")
    @ResponseBody
    public Map<String, List<PersonDto>> getPersonListEMBL() {
        return getResponseList(personService.findAll());
    }

    /**
     * This API returns a list of person entity
     *
     * @return
     */
    @GetMapping(path = "/persons")
    @ResponseBody
    public List<PersonDto> getPersonList() {
        return personService.findAll();
    }

    /**
     * This API is for getting specific person by id
     *
     * @param id is id of person entity
     * @return
     * @throws Exception
     */
    @GetMapping(path = "/persons/{id}")
    @ResponseBody
    public PersonDto getPerson(@PathVariable(required = true) Long id) throws Exception {
        return personService.find(id);
    }

    /**
     * This API is for creating new person entity
     *
     * @param personDto is dto object of person entity
     * @return
     * @throws Exception
     */
    @PostMapping(path = "/persons")
    @ResponseBody
    public PersonDto addPerson(@RequestBody(required = true) PersonDto personDto) throws Exception {
        return personService.addPerson(personDto);
    }

    /**
     * This API is for updating person entity
     *
     * @param personDto is dto object of person entity
     * @return
     * @throws Exception
     */
    @PutMapping(path = "/persons")
    @ResponseBody
    public PersonDto updatePerson(@RequestBody(required = true) PersonDto personDto) throws Exception {
        return personService.updatePerson(personDto);
    }

    /**
     * This API is for deleting person entity
     *
     * @param id is id of person entity
     * @return true in success cases
     * @throws Exception
     */
    @DeleteMapping(path = "/persons/{id}")
    @ResponseBody
    public Boolean deletePerson(@PathVariable(required = true) Long id) throws Exception {
        return personService.deletePerson(id);
    }

    /**
     * This API is for uploading data and creating person entity
     *
     * @param file is data files , contains person data
     * @return
     */
    @PostMapping("/upload")
    @ResponseBody
    public Map<String, String> uploadFile(@RequestParam("file") MultipartFile file) {
        return personService.uploadFile(file);
    }

}
