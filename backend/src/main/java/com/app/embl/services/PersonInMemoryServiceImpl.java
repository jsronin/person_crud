package com.app.embl.services;

import com.app.embl.dtos.PersonDto;
import com.app.embl.models.Person;
import com.app.embl.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.app.embl.mappers.PersonMapper.*;

/**
 * This is an in memory implementation of person service
 */
@Service
public class PersonInMemoryServiceImpl implements PersonService {

    public static String UPLOADED_FOLDER = "./uploads/";

    @Autowired
    PersonRepository personRepository;

    /**
     * This API returns a list of person entity
     *
     * @return
     */
    @Override
    public List<PersonDto> findAll() {
        return toDtoList(personRepository.findAll());
    }

    /**
     * This API is for getting specific person by id
     *
     * @param id is id of person entity
     * @return
     * @throws Exception
     */
    @Override
    public PersonDto find(Long id) throws Exception {
        return toDto(personRepository.find(id));
    }

    /**
     * This API is for creating new person entity
     *
     * @param personDto is dto object of person entity
     * @return
     * @throws Exception
     */
    @Override
    public PersonDto addPerson(PersonDto personDto) throws Exception {
        return toDto(personRepository.addPerson(toEntity(personDto)));
    }

    /**
     * This API is for adding a list of person , provided in uploaded file
     *
     * @param path is path of uploaded file
     * @return
     * @throws Exception
     */
    @Override
    public Boolean addPersonList(Path path) throws Exception {
        Boolean result = true;
        try {
            if (path == null) {
                return false;
            }
            List<String> dataLines = Files.readAllLines(path);
            if (dataLines == null || dataLines.size() < 2) {
                return false;
            }
            for (int i = 1; i < dataLines.size(); i++) {
                String[] data = dataLines.get(i).split(",");
                if (data != null && data.length == 5) {
                    Person person = new Person();
                    person.setFirstName(data[0]);
                    person.setLastName(data[1]);
                    person.setAge(data[2]);
                    person.setNationality(data[3]);
                    person.setFavouriteColour(data[4]);
                    personRepository.addPerson(person);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    /**
     * This API is for updating person entity
     *
     * @param personDto is dto object of person entity
     * @return
     * @throws Exception
     */
    @Override
    public PersonDto updatePerson(PersonDto personDto) throws Exception {
        return toDto(personRepository.updatePerson(toEntity(personDto)));
    }

    /**
     * This API is for deleting person entity
     *
     * @param id is id of person entity
     * @return true in success cases
     * @throws Exception
     */
    @Override
    public Boolean deletePerson(Long id) throws Exception {
        return personRepository.deletePerson(id);
    }

    /**
     * This API is for uploading data and creating person entity
     *
     * @param file is data files , contains person data
     * @return
     */
    @Override
    public Map<String, String> uploadFile(MultipartFile file) {
        Map<String, String> result = new HashMap<>();

        if (file == null || file.isEmpty()) {
            result.put("message", "Please select a file to upload");
            return result;
        }

        try {
            byte[] bytes = file.getBytes();
            Path uploadFolder = Paths.get(UPLOADED_FOLDER);
            if (!Files.exists(uploadFolder)) {
                Files.createDirectory(uploadFolder);
            }
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);
            result.put("message", "You successfully uploaded '" + file.getOriginalFilename() + "'");
            Boolean importResult = addPersonList(path);
            if (importResult) {
                result.put("message",
                        result.get("message") + " - " +
                                "You successfully imported data of file : '" + file.getOriginalFilename() + "'");
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.put("message", "Exception in file upload");
        }

        return result;

    }

}
