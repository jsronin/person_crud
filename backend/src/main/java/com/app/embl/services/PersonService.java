package com.app.embl.services;

import com.app.embl.dtos.PersonDto;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public interface PersonService {

    List<PersonDto> findAll();

    PersonDto find(Long id) throws Exception;

    PersonDto addPerson(PersonDto personDto) throws Exception;

    Boolean addPersonList(Path path) throws Exception;

    PersonDto updatePerson(PersonDto personDto) throws Exception;

    Boolean deletePerson(Long id) throws Exception;

    Map<String, String> uploadFile(MultipartFile file);

}
