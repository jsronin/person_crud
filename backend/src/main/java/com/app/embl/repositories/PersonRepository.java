package com.app.embl.repositories;

import com.app.embl.models.Person;

import java.util.List;

public interface PersonRepository {

    List<Person> findAll();

    Person find(Long id) throws Exception;

    Person addPerson(Person person) throws Exception;

    Person updatePerson(Person person) throws Exception;

    Boolean deletePerson(Long id) throws Exception;

}
