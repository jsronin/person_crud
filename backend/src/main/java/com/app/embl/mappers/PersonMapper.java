package com.app.embl.mappers;

import com.app.embl.dtos.PersonDto;
import com.app.embl.models.Person;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This is a mapper class for mapping dto to entity and vice versa
 */
public class PersonMapper {

    /**
     * This is to entity mapper
     *
     * @param personDto
     * @return
     */
    public static Person toEntity(PersonDto personDto) {
        Person person = new Person();
        if (personDto == null) {
            return null;
        }
        person.setId(personDto.getId());
        person.setFirstName(personDto.getFirst_name());
        person.setLastName(personDto.getLast_name());
        person.setAge(personDto.getAge());
        person.setFavouriteColour(personDto.getFavourite_colour());
        return person;
    }

    /**
     * This is to dto mapper
     *
     * @param person
     * @return
     */
    public static PersonDto toDto(Person person) {
        PersonDto personDto = new PersonDto();
        if (person == null) {
            return null;
        }
        personDto.setId(person.getId());
        personDto.setFirst_name(person.getFirstName());
        personDto.setLast_name(person.getLastName());
        personDto.setAge(person.getAge());
        personDto.setFavourite_colour(person.getFavouriteColour());
        return personDto;
    }

    /**
     * This is to entity list mapper
     *
     * @param personDtoList
     * @return
     */
    public static List<Person> toEntityList(List<PersonDto> personDtoList) {
        return personDtoList.stream().map(p -> toEntity(p)).collect(Collectors.toList());
    }

    /**
     * This is to dto list mapper
     *
     * @param personList
     * @return
     */
    public static List<PersonDto> toDtoList(List<Person> personList) {
        return personList.stream().map(p -> toDto(p)).collect(Collectors.toList());
    }

    /**
     * This is response generator mapper , for specific requested response object , {"person":[person json object]}
     *
     * @param personDtoList
     * @return
     */
    public static Map<String, List<PersonDto>> getResponseList(List<PersonDto> personDtoList) {
        Map<String, List<PersonDto>> responseList = new HashMap<>();
        responseList.put("person", personDtoList);
        return responseList;
    }

}
